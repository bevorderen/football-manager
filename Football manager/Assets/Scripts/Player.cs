﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Player : MonoBehaviour
{

    public string Name;
    public string Role;
    public double Lambda { get; set; }
    public double Morale { get; set; }

    public Player(double lambda, string name, double morale)
    {
        this.Lambda = lambda;
        this.Name = name;
        this.Morale = morale;
    }

    public void Clone()
    {
    }

    public abstract int Action(double lambda);

}
