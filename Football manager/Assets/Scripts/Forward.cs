﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Forward : Player
{
    public System.Random rnd;
    public Forward(double lambda, string name, double morale) : base(lambda, name, morale)
    {
        this.rnd = new System.Random();
        this.Role = "Нападающий";
    }

    public override int Action(double lambda)
    {
        return Convert.ToInt32((-Math.Log(rnd.NextDouble()) / Lambda));
    }

    public new Forward Clone()
    {
        return new Forward(Lambda, Name, Morale);
    }
}
