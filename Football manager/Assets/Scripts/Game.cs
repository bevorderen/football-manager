﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class Game : MonoBehaviour
{

    public int Round;
    GameObject first_group;
    GameObject second_group;

    public int[] time = new int[10];
    public int[] protection = new int[10];
    public int goal;

    public int[] default_time = new int[10];
    public int[] default_protection = new int[10];
    public int default_goal;


    private void Awake()
    {
        Round = 0;
        first_group = GameObject.Find("First group");
        second_group = GameObject.Find("Second group");
        var all = first_group.GetComponentsInChildren<Transform>();
        for (int i = 0; i < all.Length; i++)
        {
            if (all[i].name == "1")
                all[i].GetComponent<Text>().text = "Your command";
            else if (all[i].name == i.ToString() && i <= 4)
                all[i].GetComponent<Text>().text = "Command № " + i;
        }
        all = second_group.GetComponentsInChildren<Transform>();
        for (int i = 0; i < all.Length; i++)
        {
            if (all[i].name == (i + 4).ToString() && (i + 4) <= 8)
                all[i].GetComponent<Text>().text = "Command № " + (i + 4);
        }
        goal = 0;
    }

    public void Generate_goal()
    {
        for (int i = 0; i < 10; i++)
        {
            time[i] = Menu.you_players[i].Action(Menu.you_players[i].Lambda);
            protection[i] = Menu.you_goalkeeper.Action(Menu.you_goalkeeper.Lambda);

            default_time[i] = Menu.default_team[i].Action(Menu.default_team[i].Lambda);
            default_protection[i] = Menu.default_goalkeeper.Action(Menu.default_goalkeeper.Lambda);
        }
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (time[i] == default_protection[j])
                    time[i] = -1;

                if (protection[i] == default_time[j])
                    default_time[i] = -1;
            }
        }
        for (int i = 0; i < 10; i++)
        {
            if (time[i] <= 90 && time[i] >= 0)
                goal++;

            if (default_time[i] <= 90 && default_time[i] >= 0)
                default_goal++;
        }
    }

    public void New_Round()
    {
        var all = first_group.GetComponentsInChildren<Transform>();
        switch (Round)
        {
            case 0:
                Round++;
                Generate_goal();
                if (goal == default_goal)
                    goal++;
                for (int i = 0; i < all.Length; i++)
                {
                    if (all[i].name == "score_1_2")
                        all[i].GetComponent<Text>().text = "Score " + goal + ' ' + default_goal;

                    if (goal > default_goal && all[i].name == "12")
                        all[i].GetComponent<Text>().text = "Your command";
                    else if (goal < default_goal && all[i].name == "12")
                        all[i].GetComponent<Text>().text = "Command №2";
                }
                goal = 0;
                default_goal = 0;
                Generate_goal();
                if (goal == default_goal)
                    goal++;
                for (int i = 0; i < all.Length; i++)
                {
                    if (all[i].name == "score_3_4")
                        all[i].GetComponent<Text>().text = "Score " + goal + ' ' + default_goal;

                    if (goal > default_goal && all[i].name == "34")
                        all[i].GetComponent<Text>().text = "Command №3";
                    else if (goal < default_goal && all[i].name == "34")
                        all[i].GetComponent<Text>().text = "Command №4";
                }
                goal = 0;
                default_goal = 0;


                all = second_group.GetComponentsInChildren<Transform>();
                Generate_goal();
                if (goal == default_goal)
                    goal++;
                for (int i = 0; i < all.Length; i++)
                {
                    if (all[i].name == "score_8_9")
                        all[i].GetComponent<Text>().text = "Score " + goal + ' ' + default_goal;

                    if (goal > default_goal && all[i].name == "56")
                        all[i].GetComponent<Text>().text = "Command №5";
                    else if (goal < default_goal && all[i].name == "56")
                        all[i].GetComponent<Text>().text = "Command №6";
                }
                goal = 0;
                default_goal = 0;
                Generate_goal();
                if (goal == default_goal)
                    goal++;
                for (int i = 0; i < all.Length; i++)
                {
                    if (all[i].name == "score_10_11")
                        all[i].GetComponent<Text>().text = "Score " + goal + ' ' + default_goal;

                    if (goal > default_goal && all[i].name == "78")
                        all[i].GetComponent<Text>().text = "Command №7";
                    else if (goal < default_goal && all[i].name == "78")
                        all[i].GetComponent<Text>().text = "Command №8";
                }
                break;
            case 1:
                Round++;
                Generate_goal();
                if (goal == default_goal)
                    goal++;
                for (int i = 0; i < all.Length; i++)
                {
                    if (all[i].name == "win12_34")
                        all[i].GetComponent<Text>().text = "Score " + goal + ' ' + default_goal;

                    if (goal > default_goal && all[i].name == "1234")
                    {
                        for (int j = 0; j < all.Length; j++)
                        {
                            if (all[j].name == "12")
                                all[i].GetComponent<Text>().text = all[j].GetComponent<Text>().text;
                        }
                    }

                    else if (goal < default_goal && all[i].name == "1234")
                    {
                        for (int j = 0; j < all.Length; j++)
                        {
                            if (all[j].name == "34")
                                all[i].GetComponent<Text>().text = all[j].GetComponent<Text>().text;
                        }
                    }
                }
                goal = 0;
                default_goal = 0;


                all = second_group.GetComponentsInChildren<Transform>();
                Generate_goal();
                if (goal == default_goal)
                    goal++;
                for (int i = 0; i < all.Length; i++)
                {
                    if (all[i].name == "win89_1011")
                        all[i].GetComponent<Text>().text = "Score " + goal + ' ' + default_goal;

                    if (goal > default_goal && all[i].name == "5678")
                    {
                        for (int j = 0; j < all.Length; j++)
                        {
                            if (all[j].name == "56")
                                all[i].GetComponent<Text>().text = all[j].GetComponent<Text>().text;
                        }
                    }

                    else if (goal < default_goal && all[i].name == "5678")
                    {
                        for (int j = 0; j < all.Length; j++)
                        {
                            if (all[j].name == "78")
                                all[i].GetComponent<Text>().text = all[j].GetComponent<Text>().text;
                        }

                    }
                }
                break;
            case 2:
                Round++;
                Generate_goal();
                if (goal == default_goal)
                    goal++;
                for (int i = 0; i < all.Length; i++)
                {
                    if (all[i].name == "win_final")
                        all[i].GetComponent<Text>().text = "Score " + goal + ' ' + default_goal;

                    if (goal > default_goal)
                    {
                        for (int j = 0; j < all.Length; j++)
                        {
                            if (all[j].name == "1234")
                                GameObject.Find("Winner_tt").GetComponent<Text>().text = all[j].GetComponent<Text>().text;
                        }
                    }
                    else if (goal < default_goal)
                    {
                        for (int j = 0; j < all.Length; j++)
                        {
                            if (all[j].name == "5678")
                                GameObject.Find("Winner_tt").GetComponent<Text>().text = all[j].GetComponent<Text>().text;
                        }
                    }
                }
                goal = 0;
                default_goal = 0;
                break;
            default:
                SceneManager.LoadScene(0);
                break;
        }
        goal = 0;
        default_goal = 0;
    }
}
