﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Goalkeeper : Player {

    public System.Random rnd;
    public Goalkeeper(double lambda, string name, double morale) : base(lambda, name, morale)
    {
        this.rnd = new System.Random();
        this.Role = "Вратарь";
    }

    public override int Action(double lambda)
    {
        return Convert.ToInt32((-Math.Log(rnd.NextDouble()) / Lambda));
    }

    public new Goalkeeper Clone()
    {
        return new Goalkeeper(Lambda, Name, Morale);
    }
}
