﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class Menu : MonoBehaviour
{

    GameObject hire_menu;
    GameObject your_command_menu;
    GameObject main_menu;
    private string[] Players_name =
    {
        "Аленья Карлес",
        "Боатенг Кевин-Принс",
        "Дембеле Усман",
        "Коутиньо Филиппе",
        "Малком Филипе",
        "Месси Лионель",
        "Мунир Эль-Хаддади",
        "Руис Абель",
        "Суарес Луис",
        "Юмтити Самуэль",
        "Силессен Яспер"
    };

    private double[] Players_morale =
    {
        0.0005,
        0.005,
        0.0005,
        0.0005,
        0.0005,
        0.001,
        0.0005,
        0.0005,
        0.0005,
        0.007,
        0.0007
    };

    //доступные игроки для найма
    private Forward[] free_player = new Forward[10];
    private Goalkeeper free_goalkeeper;

    //команда менеджера
    public static Forward[] you_players = new Forward[10];
    public static Goalkeeper you_goalkeeper;

    //команда пк
    public static Forward[] default_team = new Forward[10];
    public static Goalkeeper default_goalkeeper;

    public int Money;
    GameObject available_players;
    GameObject your_command;
    GameObject Bank;

    private void Awake()
    {
        for (int i = 0; i < 11; i++)
        {
            if (i < 10)
            {
                free_player[i] = new Forward(Players_morale[i], Players_name[i], Players_morale[i]);
                you_players[i] = new Forward(0, "Name", 0);

                default_team[i] = new Forward(0.0007, "Name", 0.0007);

            }
            else
            {
                free_goalkeeper = new Goalkeeper(Players_morale[i], Players_name[i], Players_morale[i]);
                you_goalkeeper = new Goalkeeper(0, "Name", 0);

                default_goalkeeper = new Goalkeeper(0.007, "Name", 0.007);
            }
        }
        Money = 110000;
        Bank = GameObject.Find("Bank");
        Uppdate_Bank();
        hire_menu = transform.GetChild(2).gameObject;
        your_command_menu = transform.GetChild(1).gameObject;
        main_menu = transform.GetChild(0).gameObject;

    }

    private void Uppdate_Bank()
    {
        Bank.GetComponent<Text>().text = "Your bank " + Money + " $";
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (your_command_menu.activeSelf == true)
            {
                your_command_menu.SetActive(false);
                main_menu.SetActive(true);
            }
            else if (hire_menu.activeSelf == true)
            {
                hire_menu.SetActive(false);
                main_menu.SetActive(true);
            }
        }
    }

    public void Your_Team()
    {
        main_menu.SetActive(false);
        your_command_menu.SetActive(true);
        var My_team = GameObject.Find("Team");
        var all = My_team.GetComponentsInChildren<Transform>();
        for (int i = 2; i < 13; i++)
        {
            if (i < 12)
                all[i].GetComponent<Text>().text = you_players[i - 2].Name + "\n" + you_players[i - 2].Role + "\n" + you_players[i - 2].Morale;
            else
                all[i].GetComponent<Text>().text = you_goalkeeper.Name + "\n" + you_goalkeeper.Role + "\n" + you_goalkeeper.Morale;
        }
    }

    private void Update_player()
    {
        available_players = GameObject.Find("Available Players");
        your_command = GameObject.Find("Your Command");

        var all = available_players.GetComponentsInChildren<Transform>();
        for (int i = 2; i < 13; i++)
        {
            if (i < 12)
                all[i].GetComponent<Text>().text = free_player[i - 2].Name + "\n" + free_player[i - 2].Role + "\n" + free_player[i - 2].Morale;
            else
                all[i].GetComponent<Text>().text = free_goalkeeper.Name + "\n" + free_goalkeeper.Role + "\n" + free_goalkeeper.Morale;
        }

        all = your_command.GetComponentsInChildren<Transform>();
        for (int i = 2; i < 13; i++)
        {
            if (i < 12)
                all[i].GetComponent<Text>().text = you_players[i - 2].Name + "\n" + you_players[i - 2].Role + "\n" + you_players[i - 2].Morale;
            else
                all[i].GetComponent<Text>().text = you_goalkeeper.Name + "\n" + you_goalkeeper.Role + "\n" + you_goalkeeper.Morale;
        }
    }

    public void Hair_players()
    {
        hire_menu.SetActive(true);
        main_menu.SetActive(false);
        Update_player();
        Uppdate_Bank();
    }

    public void BuyPlayer(int number_player)
    {
        if (Money >= 10000)
        {
            if (number_player < 11)
            {
                if (free_player[number_player - 1].Name != "Name")
                {
                    you_players[number_player - 1] = free_player[number_player - 1].Clone();
                    free_player[number_player - 1].Name = "Name";
                    free_player[number_player - 1].Role = "Role";
                    free_player[number_player - 1].Morale = 0;
                    Money -= 10000;
                }
            }
            else
            {
                if (free_goalkeeper.Name != "Name")
                {
                    you_goalkeeper = free_goalkeeper.Clone();
                    free_goalkeeper.Name = "Name";
                    free_goalkeeper.Role = "Role";
                    free_goalkeeper.Morale = 0;
                    Money -= 10000;
                }
            }
            Update_player();
            Uppdate_Bank();
        }
    }

    public void SellPlayer(int number_player)
    {
        if (number_player < 11)
        {
            if (you_players[number_player - 1].Name != "Name")
            {
                free_player[number_player - 1] = you_players[number_player - 1].Clone();
                you_players[number_player - 1].Name = "Name";
                you_players[number_player - 1].Role = "Role";
                you_players[number_player - 1].Morale = 0;
                Money += 10000;
            }
        }
        else
        {
            if (you_goalkeeper.Name != "Name")
            {
                free_goalkeeper = you_goalkeeper.Clone();
                you_goalkeeper.Name = "Name";
                you_goalkeeper.Role = "Role";
                you_goalkeeper.Morale = 0;
                Money += 10000;
            }
        }
        Update_player();
        Uppdate_Bank();
    }


    public void StartGame()
    {
        var start = 0;
        for (int i = 0; i < 11; i++)
        {
            if (i < 10)
            {
                if (you_players[i].Name != "Name")
                    start++;
            }
            else
                if (you_goalkeeper.Name != "Name")
                start++;
        }
        if (start < 11)
        {
            Hair_players();
        }
        else
            SceneManager.LoadScene(1);
    }

    public void Quite()
    {
        Application.Quit();
    }
}
